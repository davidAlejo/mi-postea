@extends('layouts.app')
@section('content')

        <div class="container col-md-8 col-md-offset-2 card card-container">
            <div class="well well bs-component">
                <form class="form-horizon" method="post">

                    <!--aca vamos a devolver el la alerta del status una vez haya sido actualizado, pero si hay algun error en la actualizacion, nos mostrara un mensaje de error-->
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                    @if(session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {!! csrf_field() !!}
                    
                    <fieldset>
                        <legend>Editar Datos de Usuario</legend>
                        
                        <!--aca se visualiza un campo en donde se mostrara el usuario actual pero tambien podremos editar la informacion correspondeitne-->
                        <div class="form-gro">
                            <label for="name" class="col-lg-label">Nombre</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="name" name="name" value="{!! $usuario->name !!}">
                            </div>
                        </div>
                        
                        <!--craemos el campo donde se coloca el email. automaticamente se llama a los datos del usuario actuales-->
                        <div class="form-group">
                            <label for="email" class="col-lg-label">Email</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="email" name="email" value="{!! $usuario->email !!}">
                            </div>
                        </div>

                        <!--se crea lo que es el boton para volve a la vista de mis publicaciones y el boton para actualizar los datos del usuario una vez hayan sido modificados-->
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <a class="btn btn-primary" href="{{ route('MisPublicaciones') }}" role="button">Volver</a>
                                <button type="submit" class="btn btn-success">Actualiar Usuario</button>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <!--boton para poder eliminar la cuenta de usuario, llamaremos a lo que es la ruta usuario/edutar que se envuentra en la vista view.php, se hara uso del metodo delete al igual que como se hizo la eliminacion de las publicaciones de un usuario-->
                @auth

                <div class="text-center">
                    <form method="POST" action="{{ url("/usuario/editar") }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Eliminar Usuaio</button>
                    </form>
                </div>

                @endauth
            </div>
        </div>

@endsection