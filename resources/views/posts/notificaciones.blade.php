@extends('layouts.app')
@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center background=black" >
        <h1 class="display-5">Notificaciones</h1>
    </div>
</div>

<div class="container">
    <!-- llamamos a la funcion foreach para poder autentificar al usuario y revisar si este podee notificaciones de comentarios realizados dentro de sus publicaciones-->
    @foreach (Auth::user()->notifications as $notification)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">                   
                        <div>
                            <!-- llamamos al contenido de la base de datos en data, donde en este mismo se encuentra los valores que retorna la funcion de InvoicePaid lo mismo para las demas lineas de codigo-->
                            {{ $notification->data['content'] }} <p></p>
                            <p>ID del usuario: {{ $notification->data['post_id'] }}</p><p></p>
                        </div>
                        <div>
                            <p>ID de Notificacion: {{ $notification->notifiable_id }}</p>
                        </div>
                        <div>
                        <p>Fecha de Creacion: {{ $notification->created_at }}</p>
                        </div>
                    </h5>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection