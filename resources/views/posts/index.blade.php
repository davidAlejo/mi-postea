@extends('layouts.app')

@section('content')
<div class="container">
    @forelse ($publicaciones as $publicacion)
    <div class="row mb-4 justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="{{ action('PostController@show', $publicacion->id) }}">{{ $publicacion->title }}</a>
                    </h5>
                </div>
                <img src="{{ ($publicacion->image) }}" class="card-img-top" alt="...">
                
                <!--primero autentificamos si el usuario esta logueado, luego, vamos crear un boton en donde especificamos la ruta Mispubliaciones, pasando lo que es la variable id de la publicacion, luego haremos uso del metodo delete especifico de laravel, para eliminar la publicacion-->
                @auth
                    @if (Route::has('MisPublicaciones'))
                    <div class="text-center">
                        <form method="POST" action="{{ url("posts/{$publicacion->id}") }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar Publicacion</button>
                        </form>
                    </div>
                    @endif  
                @endauth
            </div>
        </div>
    </div>
    @empty  
    <h3>loguearse con la cuenta</h3>
    @endforelse
    <!-- Primero, contamos todas las publicaciones de la pagina actual y luego llamamos a las publicaciones que se pasaron por el controlador (en este caso la variable publicaciones) y luego combocamos una funcion links que nos dara como resultado la barra de navegacion -->
    <div class="container col-md-6 justify-content-md-center">
        <p>Numero de elementos de la pagina: {{ $publicaciones->count() }}</p>
        <br>
        {{ $publicaciones->links() }}
    </div>
</div>
@endsection
