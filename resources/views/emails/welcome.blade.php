@component('mail::message')
# Hola! {{ $user->name }}

Bienvenido a mi aplicacion Postea, Gracias por Registrarte :)

Que tenga buen dia,<br>
{{ config('app.name') }}
@endcomponent
