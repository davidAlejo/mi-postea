<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style2.css">
    <title>Document</title>
</head>

<body>
<nav class="navbar navbar-expand-sm navbar-dark sticky-top">
  <a class="navbar-brand" href="welcome">Principal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Noticias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Analisis/Opinion</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">GUIAS</a>
      </li>  
    </ul>
  </div>  
</nav>
    <div class="container">
        <article class="post col-xs-12 col-md-12 col-lg-12"">
            <h2 class="post-tittle">
                <a href="#">Dollhouse, un juego de terror noir, se muestra en un nuevo vídeo</a>
            </h2>

            <p><span class="post-fecha"><b>9 Abr 2017 - Actualizado 07/02/2019</b></span> por <span class="post-autor"><a href="#">Cristina M. Pérez</a></span></p>

            <div class="imagen1 fakeimg"><img class="" src="img/dollhouse.jpg" alt="" width="100%"></div>

            <p class="italica"><i>Estará disponible en PC y PS4 tanto físico como digital.</i></p>

            <p class="post-contenido text-justify">
                Dollhouse se presenta como un título de terror psicológico con toques noir, propios del cine negro. Desarrollado por Creazn, el título está previsto para publicarse en PC y PS4, tanto de forma física como digital.
            </p>
            <p class="post-contenido text-justify">
                Aunque llegará este año, el juego todavía no cuenta con una fecha concreta de lanzamiento. Lo que sí tiene es un nuevo y terrorífico tráiler, que podéis disfrutar a continuación:<br>
                En el modo historia de Dollhouse, los jugadores conocerán a Marie, una misteriosa mujer que en un tiempo fue conocida como "la mejor detective del mundo". Ahora, nuestra protagonista sufre amnesia y trata de forma desesperada de recuperar sus recuerdos.
            </p>
            <p class="post-contenido text-justify">
                Lo único que puede recordar es la terrible noche en la que su hermana, Emily, murió.
                Marie tendrá que buscar pistas en la tétrica Dollhouse, donde tendrá que recopilar la información basándose en el análisis minucioso del entorno que la rodea. A su vez, los jugadores tendrán que tomar decisiones cuidadosamente.

                Además, Dollhouse cuenta con un modo multijugador competitivo donde cada jugador tendrá que cumplir con un objetivo diferente. Hay 14 personajes distintos, cada uno con sus propias características.
            </p>
            <div class="contenedor-botones">
                <a href="welcome" class="btn btn-primary">Volver</a>
            </div>
        </article>
    </div>
    <div class="footer jumbotron text-center" style="margin-bottom:0">
      <h4 class="h4footer">Todos los Derechos no tan Reservados</h4>
      <p class="pfooter">By Kaisel</p>
    </div>
</body>

</html>