<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style2.css">
    <title>Document</title>
</head>

<body>
<nav class="navbar navbar-expand-sm navbar-dark sticky-top">
  <a class="navbar-brand" href="welcome">Principal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Noticias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Analisis/Opinion</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">GUIAS</a>
      </li>  
    </ul>
  </div>  
</nav>
    <div class="container">
        <article class="post col-xs-12 col-md-12 col-lg-12"">
            <h2 class="post-tittle">
                <a href="#">Todo lo que sabemos de The Last of Us Parte II (PS4)</a>
            </h2>

            <p><span class="post-fecha"><b>9 Abr 2017 - Actualizado 22/05/2020</b></span> por <span class="post-autor"><a href="#">Ramón Varela ·</a></span></p>

            <div class="imagen1 fakeimg"><img class="" src="img/theLast.jpg" alt="" width="100%"></div>

            <p class="italica"><i>La esperada secuela de The Last of Us llega en pocas semanas. Repasamos los detalles de la nueva aventura de Ellie y Joel.</i></p>

            <p class="post-contenido text-justify">
                El primer The Last of Us sin duda supuso el broche de oro para PS3, y siete años después su secuela está a punto de cerrar el ciclo de PS4, acompañado del no menos esperado Ghost of Tsushima. Ambos estarán disponibles muy pronto, a meses del lanzamiento de PS5, y prometen subir más el listón del catálogo en un año que será recordado en la industria por la cantidad de juegos de gran calidad.
            </p>
            <p class="post-contenido text-justify">
                En este avance vamos a repasar lo que ofrece The Last of Us Parte II con la información oficial, centrándonos en las novedades de los últimos meses. Sabemos que muchos jugadores ya conocen detalles clave de su trama por una reciente filtración, pero no vamos a tocar ninguno de esos aspectos en el texto, libre de spoilers. Prepárate para este regreso a un mundo postapocalíptico repleto de odio, infectados y supervivencia al límite.
            </p>
            <p class="post-contenido text-justify">
                The Last of Us Parte II se lanza el 19 de junio en PS4, tras un par de cambios de fecha. Inicialmente fue anunciado para el 21 de febrero, pero Naughty Dog retrasó la fecha unos meses más para pulir el juego, al 29 de mayo. Este plazo sí parecía poder cumplirse sin problemas –ya está terminado-, pero Sony anunció que su salida quedaba en el aire por problemas de logística provocados por la crisis del coronavirus. En la actualidad muchos países ya están comenzando a abrir negocios y relajar las limitaciones de movilidad, por lo que en junio la situación debería estar bastante más controlada.
            </p>
            <p class="post-contenido text-justify">
                Estará disponible en una edición estándar, la Digital Deluxe –al juego añade un tema dinámico, seis avatares, banda sonora digital y minilibro de ilustraciones digital-, la edición especial –juego en SteelBook, minilibro de ilustraciones, tema dinámico, avatares- y la edición para coleccionistas –juego, figura de Ellie de 30 cm, réplica del brazalete, caja SteelBook, minilibro de ilustraciones, seis pins, litografía, cinco pegatinas, tema dinámico, avatares, banda sonora digital y versión digital del minilibro. Conoce todos los detalles en cada edición en el siguiente enlace.
            </p>
            <p class="post-contenido text-justify">
                PS Store confirma que el tamaño en disco duro es de 100 GB y las versiones físicas incluirán dos discos Blu-Ray, siendo uno de los escasos juegos de la consola que requieren este espacio –Red Dead Redemption 2 fue otro caso similar-. The Last of Us Parte II no tendrá modo multijugador, a diferencia del original u otros títulos de Naughty Dog, como la saga Uncharted desde Uncharted 2, y llevan 7 años trabajando para ofrecernos en una completísima experiencia para un jugador, más larga e inmersiva que la primer parte, siendo su juego más ambicioso hasta el momento.
            </p>
            <div class="contenedor-botones">
                <a href="welcome" class="btn btn-primary">Volver</a>
            </div>
        </article>
    </div>
    <div class="footer jumbotron text-center" style="margin-bottom:0">
      <h4 class="h4footer">Todos los Derechos no tan Reservados</h4>
      <p class="pfooter">By Kaisel</p>
    </div>
</body>

</html>