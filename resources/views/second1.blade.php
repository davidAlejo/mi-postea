<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style2.css">
    <title>Document</title>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-dark sticky-top">
        <a class="navbar-brand" href="welcome">Principal</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Noticias</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Analisis/Opinion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">GUIAS</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <article class="post col-xs-12 col-md-12 col-lg-12"">
            <h2 class=" post-tittle">
            <a href="#">Dota 2 | Beastcoast ya conoce a sus rivales en la Major de Leipzig: estos son los grupos</a>
            </h2>

            <p><span class="post-fecha"><b>9 Abr 2017 - Actualizado 3 Jul 2019</b></span> por <span
                    class="post-autor"><a href="#">Eduardo Álvarez</a></span></p>

            <div class="imagen1 fakeimg"><img class="" src="img/noticiasDota.jpg" alt="" width="100%"></div>

            <p class="italica"><i>Estos son los equipos que participarán en la Major de Leipzig. | Fuente: Dream
                    League</i></p>

            <p class="post-contenido text-justify">
                Los mejores equipos de Dota 2 del mundo se reunirán en Leipzig para el segundo torneo Major de la
                temporada 2019-2020 del Dota Pro Circuit, DreamLeague Temporada 13, y ya sabemos cuáles serán los
                grupos. Los representantes peruanos de beastcoast están en el Grupo B y debutarán contra Chaos Esports
                Club de Norteamérica el sábado 18 de enero.
            </p>
            <p class="post-contenido text-justify">
                Será un torneo complicado para beastcoast, ya que no contará con Adrian ‘Wisper’ Céspedes, que ha tomado
                una pausa en su carrera por problemas de salud. Como reemplazo temporal estará el brasileño Rodrigo
                Lelis, con experiencia en dos torneos Major como parte de Pain Gaming.
            </p>
            <p class="post-contenido text-justify">
                El torneo Major repartirá US$ 1 millón en premios, además de 15 mil puntos para el Dota Pro Circuit, que
                definen la clasificación al torneo The International que este año se celebrará en Estocolmo, Suecia.
            </p>
            <p class="post-contenido text-justify">
                El anterior meta fue muy comprendido por Beastcoast, tanto así que Héctor impuso algunos estilos de
                juego con héroes como Wraith King y Lifestealer. Con la nueva versión 7.23f, no sabemos si Beastcoast ha
                podido adaptarse al parche.

                Otro punto en contra será su suplente, también debemos destacar que el equipo siempre tuvo ese tridente
                de cores K1 Hector, Chris Luck y Wisper. Esperemos que el entrenamiento en Berlin les permita conseguir
                acoplar al brasileño Lelis al equipo.

                Para finalizar, en esta ocasión Beastcoast ha llevado a un "coach" llamado Stalker, él está
                temporalmente con el conjunto y su deber será darle todas las informaciones de los rivales, estrategias
                y más en esta reñida Major.

                Estaremos informando todos los pormenores del torneo y Beastcoast en la web de Líbero Esports.
            </p>
            <div class="contenedor-botones">
                <a href="welcome" class="btn btn-primary">Volver</a>
            </div>
        </article>
    </div>
    <div class="footer jumbotron text-center" style="margin-bottom:0">
      <h4 class="h4footer">Todos los Derechos no tan Reservados</h4>
      <p class="pfooter">By Kaisel</p>
    </div>
</body>

</html>