<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style2.css">
    <title>Document</title>
</head>

<body>
<nav class="navbar navbar-expand-sm navbar-dark sticky-top">
  <a class="navbar-brand" href="welcome">Principal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Noticias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Analisis/Opinion</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">GUIAS</a>
      </li>  
    </ul>
  </div>  
</nav>
    <div class="container">
        <article class="post col-xs-12 col-md-12 col-lg-12"">
            <h2 class="post-tittle">
                <a href="#">El juego de cartas coleccionables Monster Train llega a Steam</a>
            </h2>

            <p><span class="post-fecha"><b>9 Abr 2017 - Actualizado 23/5/2020</b></span> por <span class="post-autor"><a href="#">Daniel Escandell</a></span></p>

            <div class="imagen1 fakeimg"><img class="" src="img/monsterTrain.jpg" alt="" width="100%"></div>

            <p class="italica"><i>El título ya ha llegado a PC a través de la tienda digital de Steam. Combina cartas, estilo roguelike y opciones multijugador.</i></p>

            <p class="post-contenido text-justify">
                El juego de cartas Monster Train ya está disponible en Steam con un descuento de lanzamiento por tiempo limitado del 10% sobre su precio oficial de 20,99 euros. Además, Shiny Shoe se ha unido a Mega Crit Games, desarrolladores de Slay the Spire, para ofrecer a los poseedores de ese título un 10% adicional de descuento automático en Steam.
            </p>
            <p class="post-contenido text-justify">
                Con tres campos de batalla situados en vertical, Monster Train trae una nueva perspectiva estratégica a los juegos roguelike de construcción de mazos. <br>
                Construye tu mazo de hechizos y súbditos eligiendo entre más 200 cartas. Planea tu ruta con cuidado y coloca a tus campeones de la manera más inteligente posible para vencer. Elige el clan de tus criaturas, recluta poderosas unidades y mezcla todo tipo de mejoras para crear tu propia forma de jugar y hacer tu mazo imbatible.
            </p>
            <p class="post-contenido text-justify">
                El frenético multijugador de Monster Train, llamado Hell Rush, permite que hasta ocho jugadores compitan en tiempo real para ver quién puede derrotar al cielo más rápido. Enfréntate a retos diarios con cambios especiales en el gameplay para poner a prueba tus habilidades o bien diseña tus propios retos personalizados para retar a tus amigos.<br>
                Ofrece más de 220 cartas mejorables para construir tu mazo. El usuario puede elegir entre cinco clanes de monstruos con su propia forma de jugar y campeones.
            </p>
            <div class="contenedor-botones">
                <a href="welcome" class="btn btn-primary">Volver</a>
            </div>
        </article>
    </div>
    <div class="footer jumbotron text-center" style="margin-bottom:0">
      <h4 class="h4footer">Todos los Derechos no tan Reservados</h4>
      <p class="pfooter">By Kaisel</p>
    </div>
</body>

</html>