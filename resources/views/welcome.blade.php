<!DOCTYPE html>
<html lang="en">
<head>
  <title>VideoGames</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/style.css">
  
</head>

<body>
<div class="container-portada jumbotron" style="margin-bottom:0">
  <div class="detalles">
    <h1>Pagina de videojuegos</h1>
    <p>authority for blood, blood for authority - Videogames Company</p>
  </div>
</div>

<nav class="navbar navbar-expand-sm navbar-dark">
  <a class="navbar-brand" href="#">Principal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Noticias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Analisis/Opinion</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">GUIAS</a>
      </li>    
    </ul>
  </div>  
</nav>

<div class="container" style="margin-top:30px">
  <div class="row">

    <div class="col-xs-12 col-md-6 col-lg-4">
      <a href="second1"><h2>Dota 2 | Beastcoast ya conoce a sus rivales en la Major de Leipzig: estos son los grupos</h2></a>
      <h5>Publicado el Dec 7, 2017</h5>
      <div class="fakeimg"><a href="second1"><img src="img/noticiasDota.jpg" alt="" width="100%"></a></div>
      <p>Some text..</p>
      <p class="parrafos">Los mejores equipos de Dota 2 del mundo se reunirán en Leipzig para el segundo torneo Major de la temporada 2019-2020 del Dota Pro Circuit, DreamLeague Temporada 13, y ya sabemos cuáles serán los grupos.</p>
      <br>

      <a href="second2"><h2>El juego de cartas coleccionables Monster Train llega a Steam</h2></a>
      <h5>Publicado el Dec 7, 2017</h5>
      <div class="fakeimg"><a href="second2"><img src="img/monsterTrain.jpg" alt="" width="100%"></a></div>
      <p>Some text..</p>
      <p class="parrafos">El juego de cartas Monster Train ya está disponible en Steam con un descuento de lanzamiento por tiempo limitado del 10% sobre su precio oficial de 20,99 euros. Además, Shiny Shoe se ha unido a Mega Crit Games, desarrolladores de Slay the Spire, para ofrecer a los poseedores de ese título un 10% adicional de descuento automático en Steam.</p>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-4">
      <a href="second3"><h2>Dollhouse, un juego de terror noir, se muestra en un nuevo vídeo</h2></a>
      <h5>Publicado el Dec 7, 2017</h5>
      <div class="fakeimg"><a href="second3"><img src="img/dollhouse.jpg" alt="" width="100%"></a></div>
      <p>Some text..</p>
      <p class="parrafos">Dollhouse se presenta como un título de terror psicológico con toques noir, propios del cine negro. Desarrollado por Creazn, el título está previsto para publicarse en PC y PS4, tanto de forma física como digital. Aunque llegará este año, el juego todavía no cuenta con una fecha concreta de lanzamiento. Lo que sí tiene es un nuevo y terrorífico tráiler, que podéis disfrutar a continuación:</p>
      <br>

      <a href="second4"><h2>Todo lo que sabemos de The Last of Us Parte II (PS4)</h2></a>
      <h5>Publicado el Dec 7, 2017</h5>
      <div class="fakeimg"><a href="second4"><img src="img/theLast.jpg" alt="" width="100%"></a></div>
      <p>Some text..</p>
      <p class="parrafos">El primer The Last of Us sin duda supuso el broche de oro para PS3, y siete años después su secuela está a punto de cerrar el ciclo de PS4, acompañado del no menos esperado Ghost of Tsushima. Ambos estarán disponibles muy pronto, a meses del lanzamiento de PS5, y prometen subir más el listón del catálogo en un año que será recordado en la industria por la cantidad de juegos de gran calidad.</p>
    </div>

    <div class="aside col-xs-12 col-md-12 col-lg-3 ml-md-auto">
      <div class="aside-group">
        <h2>Extras</h2>
        <h5>Los barcos italianos pronto se sumarán a War Thunder con más de 20 buques diferentes</h5>
        <div class="fakeimg"><img src="img/barcos.jpg" alt="" width="100%"></div>
        <p class="parrafo-aside">Gaijin Entertainment ha anunciado que la Armada Italiana llegará al juego de acción militar en línea War Thunder muy pronto.</p>
      </div>

      <div class="aside-group">
        <h5>La compañía china Tencent adquiere los derechos de System Shock 3</h5>
        <div class="fakeimg"><img src="img/systemShock.jpg" alt="" width="100%"></div>
        <p class="parrafo-aside">Los desarrolladores Otherside Entertaimment anuncian que el conglomerado asiático dirigirá el desarrollo del título.</p>
      </div>

      <div class="aside-group">
        <h3>Otros</h3>
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#">Pag principal</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Foros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Consultas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">enterteiment</a>
          </li>
        </ul>
      </div>
      <hr class="d-sm-none">
    </div>
  </div>
</div>

  <div class="jumbo jumbotron text-center col-md-12" style="margin-bottom:0">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/prototipe.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/sonic.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/dota2.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="footer jumbotron text-center" style="margin-bottom:0">
  <h4>Todos los Derechos Reservados</h4>
  <p>By Kaisel</p>
</div>

</body>
</html>