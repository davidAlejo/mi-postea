<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/posts');
Route::redirect('/home', '/posts');
Route::get('/posts', 'PostController@index');
Route::get('/posts/create', 'PostController@create')->name('crearPublicacion');
Route::post('/posts', 'PostController@store');
//Route::get('/posts/myPosts', 'PostController@userPosts');
Route::get('/posts/myPosts', 'PostController@userPosts')->name('MisPublicaciones');
Route::delete('/posts/{id}', 'PostController@destroy');
Route::get('/posts/{id}', 'PostController@show')->name('post');
//Route::get('/posts/{id}', 'PostController@destroy')->name('posts.destroy');
Route::post('/comments', 'CommentController@store');
Route::get('usuario/editar', 'UserController@edit')->name('editarUsuario');
Route::post('usuario/editar', 'UserController@update');
Route::delete('/usuario/editar', 'UserController@destroy')->name('eliminarUsuario');
//Route::get('/posts/{id}', 'PostController@destroy')->name('posts.destroy');

// ----------------------- NOTIFICACIONES --------------------------    

Route::get('/notificaciones', function(){
    return view('.posts.notificaciones');
})->name('notificaciones');

Auth::routes();


/*Route::get('second1', function () {
    return view('second1');
});

Route::get('second2', function () {
    return view('second2');
});

Route::get('second3', function () {
    return view('second3');
});

Route::get('second4', function () {
    return view('second4');
});

Route::get('welcome', function () {
    return view('welcome');
});*/
