<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
//use Illuminate\Http\File;
//use Illuminate\Support\Facades\Storage;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'image' => $faker->imageUrl(400, 300),
        'content' => $faker->paragraph(3),
    ];

//    $imagen = $faker->image();
//    $archivoImagen = new File($imagen);

//    return [
//        'image' => Storage::disk('public')->putFile('imagenes', $archivoImagen),
//    ];
});
