<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{ 
    /**
     * Seed the application's database.
     *e
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserCollectionSeeder::class);
        // $this->call(PostsCollectionSeeder::class);
    }
}
