<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvoicePaid extends Notification
{
    use Queueable;
    #creacion de atributos de clase
    public $id_post;
    public $id_usuario;

    public function __construct($id_post, $id_usuario)
    {
        #se recibe lo que es el id_post para guardarlo en el atributo de clase
        $this->id_post=$id_post;
        #se recibe lo que es el id_usuario para guardarlo en el atributo de clase
        $this->id_usuario=$id_usuario;
    }

    public function via($notifiable)
    {  
        #cambiamos el retorno para que pueda guardarse en la base de datos
        return ['database'];
    }
    
    #funcion para devolver el contenido de la notificacion en la base de datos
    public function toDatabase($notifiable){
        return [
            'content' => "Alguién comentó tu post",
            'post_id' => $this->id_post,
            'user_id' => $this->id_usuario,
        ];
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
