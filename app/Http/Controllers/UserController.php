<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        
        //$publicaciones = Post::where('user_id', '=', $user_id)->paginate();
        //return view('posts.index', compact('publicaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     
    public function edit(User $user)
    {
        //
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     
    public function update(Request $request, User $user)
    {
        //
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     
    public function destroy(User $user)
    {
        //
    }*/

    public function edit()
    {
        #primero, capturamos el id del usuario y lo guardamos dentro de una variable
        #luego, realizamos la consulta para buscar en la base de datos el id del usuario seleccionado
        #al final, retornamos la vista edit, devolviendo la variable usuarios
        $user_id = Auth::id();
        $usuario = User::find($user_id);
        return view('usuario.edit', compact('usuario'));
    }

    public function update(Request $request)
    {
        #capturamos el id del usuario y lo guardamos dentro de una variable
        #al final, realizamos la bosqueda en la base de datos pasando el id caputado anteriormente
        #luego, hacemos referencia al nombre del usuario que obtenemos de la base de datos,
        #lo mismo con el id
        $user_id = Auth::id();
        $usuario = User::find($user_id);
        $usuario->name = $request->get('name');
        $usuario->email = $request->get('email');
        
        #si el status que se obtiene es no es nulo, devuelve 0, por el contrario, devuelve 1
        if($request->get('status') != null){
            $usuario->status = 0;
        }else{
            $usuario->status = 1;
        }

        #al final guardamos los nuevos datos del usuario
        $usuario->save();

        #retornamos y nos redirigimos a la funcion edit de este controlador, y devolvemos el status
        return redirect(action('UserController@edit'))->with('status', 'Los datos del usuario han sido actualizados');
    }

    public function destroy()
    {
        $user_id = Auth::id();
        $usuario = User::find($user_id);
        $usuario->delete();
        return redirect('/home');
    }    
}
