<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use Illuminate\Database\Eloquent\Model;
/*
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Post;
use DateTime;
use Carbon\Carbon;*/

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        /* en esta seccion, vamos a listar todas las publicaciones
        y las vamos a listar 10 publicaciones por pagina, todo esto guardado dentro de una variable publicaciones, al final, retornareos la vista index junto a las publicaciones*/
        $publicaciones = Post::paginate(10);
        return view('posts.index', compact('publicaciones'));
        //$user_id = Auth::id();
        //$publicacionespg = Post::where('user_id', '=', $user_id)->paginate(10);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function show($id)
    {
        return view('posts.postUnico', ['post' => Post::find($id)]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required:max:120',
            'image' => 'required|image|mimes:jpeg,png,jpg|:max2048',
            'content' => 'required:max:2200',
        ]);

        $image = $request->file('image');
        $imageName = time().$image->getClientOriginalName();
        //$imageName = $request->file('image')->store('posts/' . Auth::id(), 'public');
        $title = $request->get('title');
        $content = $request->get('content');

        $post = $request->user()->posts()->create([
            'title' => $title,
            'image' => 'img/' . $imageName,
            'content' => $content,
        ]);

        $request->image->move(public_path('img'), $imageName);
        
        return redirect()->route('post', ['id' => $post->id]);
    }

    public function userPosts()
    {
        #obtenemos el id o luego comparamos el id con el id del usuario actual,
        #luego, retornamos la vista incex y la variable publicaciones
        $user_id = Auth::id();
        $publicaciones = Post::where('user_id', '=', $user_id)->paginate(10);
        return view('posts.index', compact('publicaciones'));
    }

    public function destroy($id)
    {
        #lo que hacemmos es buscar el id de la publicacion  luego eliminar lo que es la publicacion
        #al final retornamos la misma ruta myPost actual en donde se encuentra el usuario
        $post= Post::find($id);
        $post->delete();
        return redirect('/posts/myPosts');
    }
}
?>