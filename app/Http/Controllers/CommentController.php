<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Notifications\InvoicePaid;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required:max:250',
        ]);
            
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->content = $request->get('content');
        
        /* primero, obtener el id del post buscandolo en la base de datos
        para guardarlo en la base de datos, luego, buscamos el id del dueño de la publicacion 
        y lo vamos a guardar dentro de una variable dueño_id, buscamos y comparamos 
        si el id de la publicacion es igual o si existe dentro de la colleccion usuarios 
        de la base de datos, para finalizar, vamos vamos a guardar el comentario pasando 
        como parametro la informacion guardada dentro de la variable comment */
        $post = Post::find($request->get('post_id'));
        $dueño_id = $post->user_id;
        $dueño = User::where('_id', '=', $dueño_id)->first();
        $post->comments()->save($comment);
        
        /*esta linea de codigo es para hacer una notificacion al dueño de la 
        publicacion haciendo una invocacion a la clase InvoicePaid, pasando como parametro el id 
        del post e udo del usuario de comentario*/
        $dueño->notify(new InvoicePaid($post->id, $comment->user_id));

        return redirect()->route('post', ['id' => $request->get('post_id') ]);   
    }
}
?>